import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Icon from '@material-ui/core/Icon';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import TextField from  '@material-ui/core/TextField';
import { useEffect, useState } from 'react';

const useStyles = makeStyles({
root: {
    minWidth:275,
},
title: {
    fontSize: 15,
},
pos: {
    marginTop: 12,
    marginBottom: 0,
},
marginl:{
  marginLeft: 'auto !important',
},
});

export default function CardTeacher ({ datos, index, onDelete }) {
const classes = useStyles();
const [editar, setEditar] = useState(false);
const [tec, setTec] = useState(datos)

const toogleEditar =  () => {
    //editar
    setEditar(!editar);
};

const handleChange = (e) => {
    // actualizar
    setTec({
        ...tec,
        [e.target.name] : e.target.value,
    });
};

const handleGuardar = () =>{
    console.log("Guardado");
    setEditar(false);
};
 const handleEliminar = () =>{
//Eliminar
 if (onDelete) {
     //se invoca la funcion
     onDelete(index);
 }
 };

 useEffect(() => {
    setTec({ ...datos })
 }, [datos, index]);

return(
<Card className={classes.root} variant="outlined">
    <CardContent>
    {!editar && (
        <>
        <Typography className={classes.title}>
            Profesor
        </Typography>
        <Typography variant="h5" component="h2">
            {tec.nombre}
        </Typography>
        </>
    )}

     {editar && (
    <TextField 
      label="Nombre"
      name="nombre"
      value={tec.nombre}
      onChange={handleChange}
    />
     )}

        {!editar && (
        <>
        <Typography className={classes.pos}>
           Materia
        </Typography>
        <Typography variant="h6" component="p">
            {tec.materia}
        </Typography>
         </>
        )}

        {editar && (
       <TextField 
      label="Materia"
      name="materia"
      value={tec.materia}
      className={classes.pos}
      onChange={handleChange}
    />
        )}

       {!editar && (
        <>
        <Typography className={classes.pos}>
            Horario
        </Typography>
        <Typography variant="h6" component="p">
            {tec.horario}
        </Typography>
        </>
        )}

         {editar && (
          <TextField 
          label="Horario"
          name="horario"
          value={tec.horario}
          className={classes.pos}
          onChange={handleChange}
        />
        )}

    </CardContent>


    <CardActions>
        {!editar && (
      <IconButton color="primary" onClick={toogleEditar}>
      <Icon size="small">edit</Icon>
      </IconButton>
        )}

        {editar && (
        <IconButton color="primary" onClick={handleGuardar}>
        <Icon size="small">save</Icon>
        </IconButton>
        )}
        
        <IconButton color="secondary" 
        className={classes.marginl}
        onClick={handleEliminar}
        >
            <Icon size="small">delete</Icon>
        </IconButton>
    </CardActions>

</Card>
);
};